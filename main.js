
function saudac(){
    let agora = new Date()
    let hora = agora.getHours()
    let saud = document.querySelector("#saudacao")
    if (hora < 12) {
        saud.innerHTML = 'Bom dia! Como está?'

    }else if (hora < 18) {
        saud.innerHTML = 'Boa tarde! Como está?'
    }else if (hora < 24) {
        saud.innerHTML = 'Boa noite! Como está?'
    }
}
saudac()

const main = document.querySelector('#main')
const header = document.querySelector('.header')
const divmen2 = document.querySelector('#butcor')
const folha = document.querySelector('#folha')
const eduqc = document.querySelector("#eduqc")
const odisseia = document.querySelector('#odisseia')
const h2ultimas = document.querySelector('#ultimas')
const h2palestra = document.querySelector("#h2palestra")
const h2programa = document.querySelector('#programa')
const h2video = document.querySelector('#video')

const dark = document.querySelector('#darkmode')
dark.addEventListener('change', function(){
    if(this.checked){
       pretao()
    }else{
      recarregarPagina()
    } 
})


function recarregarPagina(){
    window.location.reload()
}
function pretao(){
    document.body.style.backgroundColor= 'black'
    document.body.style.color = 'var(--branco)'
    main.style.backgroundColor = 'black'
    folha.style.backgroundColor = 'black'
    eduqc.style.backgroundColor = 'black'
    odisseia.style.backgroundColor = 'black'
    h2ultimas.style.background = 'black'
    h2palestra.style.background = 'black'
    h2programa.style.background = 'black'
    h2video.style.background= 'black'
    divmen2.style.background= 'black'
}

let btn = document.getElementById("btn")


btn.addEventListener('click', function (){
   const menu = document.getElementById("menu") 
   menu.classList.toggle('active')
    
})

let sobre = document.querySelector("#sobre")

sobre.addEventListener('click', function(){
    btn.trigger('click')
})

